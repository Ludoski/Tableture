import javax.swing.*;
import java.awt.event.*;
import java.awt.*;




public class MaliGUI extends JFrame {
	
	
	public MaliGUI(){
		
		Container kontejner=this.getContentPane();
		kontejner.setLayout(new GridLayout(1,2,5,5));
		
		JButton bass=new JButton("Bas");
		JButton gitara=new JButton("Gitara");
		kontejner.add(bass);
		kontejner.add(gitara);
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(200, 100);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		
		
		bass.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Lista lista=new Lista();
				Fajl fajl=new Fajl();
				GUI gui=new GUI("bas");
				gui.setLista(lista);
				gui.setFajl(fajl);
				MaliGUI.this.dispose();
			}
		});

		gitara.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Lista lista=new Lista();
				Fajl fajl=new Fajl();
				GUI gui=new GUI("gitara");
				gui.setLista(lista);
				gui.setFajl(fajl);
				MaliGUI.this.dispose();
			}
		});
	}

}




