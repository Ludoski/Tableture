import java.util.LinkedList;

import javax.swing.JFormattedTextField;
import javax.swing.JTextArea;

public class Lista {

	private LinkedList<Pragovi> listaPragova = new LinkedList<Pragovi>();
	
	public LinkedList<Pragovi> getListaPragova(){
		return listaPragova;
	}
	
	public void setListaPragova(LinkedList<Pragovi> listaPragova){
		this.listaPragova=listaPragova;
	}
	
	public void prikaziListu(JTextArea ispisTablature,JFormattedTextField indeks){
		ispisTablature.setText("");
		String brojac = " ";
		String e1Str = "";
		String h2Str = "";
		String g3Str = "";
		String d4Str = "";
		String a5Str = "";
		String e6Str = "";
		
		int brojObjekata=0;
		
		for (Pragovi prag:listaPragova){
			brojObjekata++;
			if (brojObjekata %5 == 0){
				if (brojObjekata<100) {
					if (brojObjekata==Integer.parseInt(indeks.getText())){
						brojac=brojac + "@  ";
					}
					else brojac= brojac+brojObjekata+" ";
				}
				else {
					if (brojObjekata==Integer.parseInt(indeks.getText())){
						brojac=brojac + "@  ";
					}
					else brojac= brojac+brojObjekata;}
				}
			else{
				if (brojObjekata==Integer.parseInt(indeks.getText())){
					brojac=brojac + "@  ";
				}
				else brojac = brojac + "   ";
			}
			
			e1Str=e1Str+prag.getE1()+" ";
			h2Str=h2Str+prag.getH()+" ";
			g3Str=g3Str+prag.getG()+" ";
			d4Str=d4Str+prag.getD()+" ";
			a5Str=a5Str+prag.getA()+" ";
			e6Str=e6Str+prag.getE6()+" ";
			
			if (brojObjekata%40==0) {
				ispisTablature.append(brojac + "\n");
				ispisTablature.append(e1Str + "\n");
				ispisTablature.append(h2Str + "\n");
				ispisTablature.append(g3Str + "\n");
				ispisTablature.append(d4Str + "\n");
				ispisTablature.append(a5Str + "\n");
				ispisTablature.append(e6Str + "\n");
				ispisTablature.append("\n");
				brojac= "";
				e1Str = "";
				h2Str = "";
				g3Str = "";
				d4Str = "";
				a5Str = "";
				e6Str = "";
			}
		}
		ispisTablature.append(brojac + "\n");
		ispisTablature.append(e1Str + "\n");
		ispisTablature.append(h2Str + "\n");
		ispisTablature.append(g3Str + "\n");
		ispisTablature.append(d4Str + "\n");
		ispisTablature.append(a5Str + "\n");
		ispisTablature.append(e6Str + "\n");
		//indeks.setValue(brojObjekata+1);
		
	}
	
	public void unesiUListu(Pragovi prag,JFormattedTextField indeks){
		try{
			if((Integer.parseInt(indeks.getText())-1)<(listaPragova.size())){
				listaPragova.add(Integer.parseInt(indeks.getText())-1, prag);
				indeks.setText(Integer.toString(listaPragova.size()+1));	
			}
			else {
				if((Integer.parseInt(indeks.getText())-1)==(listaPragova.size())){
					listaPragova.add(prag);
					indeks.setText(Integer.toString(listaPragova.size()+1));
				}
				else{
					indeks.setText(Integer.toString(listaPragova.size()+1));
				}
			}
		} 
		catch (Exception e) {
			indeks.setText(Integer.toString(listaPragova.size()+1));
		}
			
	}
	
	public void izmeniListu(Pragovi prag,JFormattedTextField indeks){
		try{
			if((Integer.parseInt(indeks.getText())-1)<(listaPragova.size())){
				listaPragova.set(Integer.parseInt(indeks.getText())-1, prag);
				indeks.setText(Integer.toString(listaPragova.size()+1));
			}
			else {
				indeks.setText(Integer.toString(listaPragova.size()+1));
			}
		} 
		catch (Exception e) {
			indeks.setText(Integer.toString(listaPragova.size()+1));
		}
		
	}
		
	
	public void obrisiUnos(JFormattedTextField indeks){
		try{
			if((Integer.parseInt(indeks.getText())-1)<(listaPragova.size())){
				listaPragova.remove(Integer.parseInt(indeks.getText())-1);
				indeks.setText(Integer.toString(listaPragova.size()+1));
			}
			else {
				indeks.setText(Integer.toString(listaPragova.size()+1));
			}
		} 
		catch (Exception e) {
			indeks.setText(Integer.toString(listaPragova.size()+1));
		}
		
	}
}
