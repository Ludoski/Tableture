import java.util.LinkedList;

import javax.swing.SingleSelectionModel;

import java.io.*;

public class Fajl {

	public void newTablature(){
		
	}
	
	public LinkedList<Pragovi> openTablature(File odabraniFajl){
		 LinkedList<Pragovi> e = null;
		try {
	         FileInputStream fileIn = new FileInputStream(odabraniFajl);
	         ObjectInputStream in = new ObjectInputStream(fileIn);
             e = (LinkedList<Pragovi>) in.readObject();
	         in.close();
	         fileIn.close();
	    }
		catch(IOException i) {
	         i.printStackTrace();
	    }
		catch(ClassNotFoundException c) {
	         c.printStackTrace();
	    }
		return e;
	}
	
	public void saveTablature(File odabraniFajl, LinkedList<Pragovi> listaPragova){
		try {
	         String provera = odabraniFajl.toString();
			 provera = provera.substring(provera.length()-5,provera.length());
			 if (provera.equals(".doko")){
				 provera = odabraniFajl.toString();
			 }
			 else{
				  provera = odabraniFajl.toString() + ".doko";
			 }
			 FileOutputStream fileOut = new FileOutputStream(provera);
	         ObjectOutputStream out = new ObjectOutputStream(fileOut);
	         out.writeObject(listaPragova);
	         out.close();
	         fileOut.close();
	    }
		catch(IOException i) {
	         i.printStackTrace();
		}
	}
	
	public void printTablature(File odabraniFajl, LinkedList<Pragovi> listaPragova){
		try{
			String provera = odabraniFajl.toString();
			 provera = provera.substring(provera.length()-4,provera.length());
			 if (provera.equals(".txt")){
				 provera = odabraniFajl.toString();
			 }
			 else{
				  provera = odabraniFajl.toString() + ".txt";
			 }
			PrintWriter writer = new PrintWriter(provera);
			String brojac = " ";
			String e1Str = "";
			String h2Str = "";
			String g3Str = "";
			String d4Str = "";
			String a5Str = "";
			String e6Str = "";
			
			int brojObjekata=0;
			
			for (Pragovi prag:listaPragova){
				brojObjekata++;
				if (brojObjekata %5 == 0){
					if (brojObjekata<100) brojac= brojac+brojObjekata+" ";
					else brojac= brojac+brojObjekata;}
				else brojac = brojac + "   ";
				
				e1Str=e1Str+prag.getE1()+" ";
				h2Str=h2Str+prag.getH()+" ";
				g3Str=g3Str+prag.getG()+" ";
				d4Str=d4Str+prag.getD()+" ";
				a5Str=a5Str+prag.getA()+" ";
				e6Str=e6Str+prag.getE6()+" ";
			
				if (brojObjekata%40==0) {
					writer.println(brojac);
					writer.println(e1Str);
					writer.println(h2Str);
					writer.println(g3Str);
					writer.println(d4Str);
					writer.println(a5Str);
					writer.println(e6Str);
					writer.println("");
					brojac= "";
					e1Str = "";
					h2Str = "";
					g3Str = "";
					d4Str = "";
					a5Str = "";
					e6Str = "";
				}
			}
			writer.println(brojac);
			writer.println(e1Str);
			writer.println(h2Str);
			writer.println(g3Str);
			writer.println(d4Str);
			writer.println(a5Str);
			writer.println(e6Str);
		    writer.close();
		}
		catch (IOException err) {
		
		}

	}
}
