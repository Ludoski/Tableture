import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.filechooser.*;
import java.awt.event.*;
import java.awt.*;
import java.io.*;
import java.util.LinkedList;




public class GUI extends JFrame{

	private JTextArea ispisTablature=new JTextArea(24,1);
	private JFormattedTextField indeks = new JFormattedTextField("1");
	private JComboBox e1 = new JComboBox();
	private JComboBox h2 = new JComboBox();
	private JComboBox g3 = new JComboBox();
	private JComboBox d4 = new JComboBox();
	private JComboBox a5 = new JComboBox();
	private JComboBox e6 = new JComboBox();
	private Lista lista;
	private Fajl fajl;
	
	
	public GUI(String izborInstrumenta){
			
		Container kontejner=this.getContentPane();
		kontejner.setLayout(new BorderLayout());
		
		
		//Definisanje menija
		JMenuBar meniBar = new JMenuBar();
		JMenu file = new JMenu("File");
		file.setMnemonic(KeyEvent.VK_F);
		JMenuItem newTablature = new JMenuItem("New Tablature");
		newTablature.setMnemonic(KeyEvent.VK_N);
		file.add(newTablature);
		JMenuItem openTablature = new JMenuItem("Open Tablature");
		openTablature.setMnemonic(KeyEvent.VK_O);
		file.add(openTablature);
		JMenuItem saveTablature = new JMenuItem("Save Tablature");
		saveTablature.setMnemonic(KeyEvent.VK_S);
		file.add(saveTablature);
		
		JMenuItem printTablature = new JMenuItem("Print to txt");
		printTablature.setMnemonic(KeyEvent.VK_P);
		file.add(printTablature);
		JMenuItem exit = new JMenuItem("Exit");
		exit.setMnemonic(KeyEvent.VK_X);
		file.add(exit);
		meniBar.add(file);
		setJMenuBar(meniBar);
		
		newTablature.addActionListener((ActionEvent event) -> {
			LinkedList<Pragovi> gLista = new LinkedList<Pragovi>();
			lista.setListaPragova(gLista);
			lista.prikaziListu(ispisTablature, indeks);
			e1.setSelectedIndex(0);
			h2.setSelectedIndex(0);
			g3.setSelectedIndex(0);
			d4.setSelectedIndex(0);
			a5.setSelectedIndex(0);
			e6.setSelectedIndex(0);
			MaliGUI maligui=new MaliGUI();
			GUI.this.dispose();
        });
		
		openTablature.addActionListener((ActionEvent event) -> {
			JFileChooser birajFajl = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter(".doko", "doko");
			birajFajl.setFileFilter(filter);
			int returnVal = birajFajl.showOpenDialog(kontejner);
	        if (returnVal == JFileChooser.APPROVE_OPTION) {
	            File odabraniFajl = birajFajl.getSelectedFile();
	            LinkedList<Pragovi> gLista = new LinkedList<Pragovi>();
	            gLista = fajl.openTablature(odabraniFajl);
	            lista.setListaPragova(gLista);
	            lista.prikaziListu(ispisTablature, indeks);
	        } 
	        else {
	        }
        });
		
		saveTablature.addActionListener((ActionEvent event) -> {
			JFileChooser birajFajl = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter(".doko", "doko");
			birajFajl.setFileFilter(filter);
			int returnVal = birajFajl.showSaveDialog(kontejner);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File odabraniFajl = birajFajl.getSelectedFile();
                //This is where a real application would save the file.
                LinkedList<Pragovi> gLista = new LinkedList<Pragovi>();
                gLista = lista.getListaPragova();
                fajl.saveTablature(odabraniFajl, gLista);
            } 
            else {
                
            }
        });
		printTablature.addActionListener((ActionEvent event) -> {
			JFileChooser birajFajl = new JFileChooser();
			FileNameExtensionFilter filter = new FileNameExtensionFilter(".txt", "txt");
			birajFajl.setFileFilter(filter);
			int returnVal = birajFajl.showSaveDialog(kontejner);
            if (returnVal == JFileChooser.APPROVE_OPTION) {
                File odabraniFajl = birajFajl.getSelectedFile();
                //This is where a real application would save the file.
                LinkedList<Pragovi> gLista = new LinkedList<Pragovi>();
                gLista = lista.getListaPragova();
                fajl.printTablature(odabraniFajl, gLista);
            } 
            else {
                
            }
        });
		
	
	
		
		exit.addActionListener((ActionEvent event) -> {
            System.exit(0);
        });
		
		ispisTablature.setFont(new Font("monospaced", Font.PLAIN, 14));
		ispisTablature.setEditable(false); // set textArea non-editable
		JScrollPane scroll = new JScrollPane(ispisTablature);
		scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
		kontejner.add(scroll, BorderLayout.NORTH);
		
		Container unosPragova = new Container();
		unosPragova.setLayout(new GridLayout(7,2,5,5));
		String[] sadrzajBoksa= {"--", "-x", "-0", "-1", "-2", "-3", "-4", "-5", "-6", "-7", "-8", "-9", "10",
								"11", "12", "13", "14", "15", "16", "17", "18", "19", "20",
								"21", "22", "23", "24"};
		for (int i=0; i<26; i++){
			e1.addItem(sadrzajBoksa[i]);
			h2.addItem(sadrzajBoksa[i]);
			g3.addItem(sadrzajBoksa[i]);
			d4.addItem(sadrzajBoksa[i]);
			a5.addItem(sadrzajBoksa[i]);
			e6.addItem(sadrzajBoksa[i]);
		}
		
		unosPragova.add(indeks);
		unosPragova.add(new JLabel("Indeks"));
		e1.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(e1);
		JLabel e1lbl=new JLabel("e1");
		unosPragova.add(e1lbl);
		h2.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(h2);
		JLabel h2lbl=new JLabel("h");
		unosPragova.add(h2lbl);
		g3.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(g3);
		unosPragova.add(new JLabel("g"));
		d4.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(d4);
		unosPragova.add(new JLabel("d"));
		a5.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(a5);
		unosPragova.add(new JLabel("a"));
		e6.setMaximumRowCount(sadrzajBoksa.length);
		unosPragova.add(e6);
		unosPragova.add(new JLabel("e6"));
		kontejner.add(unosPragova, BorderLayout.WEST);
		
		switch (izborInstrumenta) {
		case "bas":
			e1.setVisible(false);
			e1lbl.setVisible(false);
			h2.setVisible(false);
			h2lbl.setVisible(false);
			break;
		}
		
		
		Container unosLinija = new Container();
		unosLinija.setLayout(new GridLayout(1,2,5,5));
		JButton linijeH=new JButton("-");
		JButton linijeV=new JButton("|");
		unosLinija.add(linijeH);
		unosLinija.add(linijeV);
		kontejner.add(unosLinija, BorderLayout.CENTER);
		
		Container izvrsnaDugmad=new Container();
		izvrsnaDugmad.setLayout(new GridLayout(3,1,5,5));
		JButton unesi=new JButton("Unesi");
		JButton izmeni=new JButton("Izmeni");
		//JButton umetni=new JButton("Umetni");
		JButton obrisi=new JButton("Obri�i");
		izvrsnaDugmad.add(unesi);
		izvrsnaDugmad.add(izmeni);
		//izvrsnaDugmad.add(umetni);
		izvrsnaDugmad.add(obrisi);
		kontejner.add(izvrsnaDugmad, BorderLayout.EAST);
		
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		this.setSize(1000,700);
		this.setLocationRelativeTo(null);
		this.setVisible(true);
		this.setResizable(false);
		
				
		unesi.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Pragovi prag = new Pragovi();
				
				switch (izborInstrumenta){
				case "bas":
					prag.setE1("  ");
					prag.setH("  ");
					break;
				case "gitara":
					prag.setE1((String) e1.getSelectedItem());
					prag.setH((String) h2.getSelectedItem());
					break;
				}
				
				prag.setG((String) g3.getSelectedItem());
				prag.setD((String) d4.getSelectedItem());
				prag.setA((String) a5.getSelectedItem());
				prag.setE6((String) e6.getSelectedItem());
				
				lista.unesiUListu(prag,indeks);
				lista.prikaziListu(ispisTablature, indeks);
			}
		});

		
		
		
		izmeni.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Pragovi prag = new Pragovi();
				
				switch (izborInstrumenta){
				case "bas":
					prag.setE1("  ");
					prag.setH("  ");
					break;
				case "gitara":
					prag.setE1((String) e1.getSelectedItem());
					prag.setH((String) h2.getSelectedItem());
					break;
				}
				
				prag.setG((String) g3.getSelectedItem());
				prag.setD((String) d4.getSelectedItem());
				prag.setA((String) a5.getSelectedItem());
				prag.setE6((String) e6.getSelectedItem());
				
				lista.izmeniListu(prag,indeks);
				lista.prikaziListu(ispisTablature, indeks);
			
			}
		});
		
		obrisi.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
								
				lista.obrisiUnos(indeks);
				lista.prikaziListu(ispisTablature, indeks);
			
			}
		});
		
		linijeV.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Pragovi prag = new Pragovi();
				
				switch (izborInstrumenta){
				case "bas":
					prag.setE1("  ");
					prag.setH("  ");
					break;
				case "gitara":
					prag.setE1("||");
					prag.setH("||");
					break;
				}
				
				prag.setG("||");
				prag.setD("||");
				prag.setA("||");
				prag.setE6("||");
				
				lista.unesiUListu(prag,indeks);
				lista.prikaziListu(ispisTablature, indeks);
			
			}
		});
		
		linijeH.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) {
				Pragovi prag = new Pragovi();
				
				switch (izborInstrumenta){
				case "bas":
					prag.setE1("  ");
					prag.setH("  ");
					break;
				case "gitara":
					prag.setE1("--");
					prag.setH("--");
					break;
				}
								
				prag.setG("--");
				prag.setD("--");
				prag.setA("--");
				prag.setE6("--");
				
				lista.unesiUListu(prag,indeks);
				lista.prikaziListu(ispisTablature, indeks);
			
			}
		});
		
		indeks.addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
			
			@Override
			public void keyReleased(KeyEvent arg0) {
				// TODO Auto-generated method stub
				if ((arg0.getKeyCode()==KeyEvent.VK_ENTER) && (arg0.getSource()==indeks)) {lista.prikaziListu(ispisTablature, indeks);}
				
				//System.out.println(tfid.getText());
			}
			
			@Override
			public void keyPressed(KeyEvent arg0) {
				// TODO Auto-generated method stub
				
			}
		});
		
	}

	public void setLista(Lista lista){
		this.lista=lista;
	}
	
	public void setFajl(Fajl fajl){
		this.fajl=fajl;
	}
}
